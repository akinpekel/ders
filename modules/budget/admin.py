from django.contrib import admin
from modules.budget.models import settingData,settings,budgetData,Budgets


class settingsDataInline(admin.StackedInline):
    model = settingData
    extra = 1


class settingsAdmin(admin.ModelAdmin):
    inlines = [settingsDataInline]



admin.site.register(settings,settingsAdmin)


class budgetDataInline(admin.StackedInline):
    model = budgetData
    extra = 1


class BudgetsAdmin(admin.ModelAdmin):
    inlines = [budgetDataInline]



admin.site.register(Budgets,BudgetsAdmin)