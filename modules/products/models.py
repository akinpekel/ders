from django.db import models


# Create your models here.


class products(models.Model):
    product_name = models.TextField(verbose_name="Ürün İsmi", blank=True)
    product_code = models.CharField(max_length=20, verbose_name="Ürün Kodu")
    product_price = models.DecimalField(verbose_name="Ürün Fiyatı", max_digits=8, decimal_places=2)
    status = models.BooleanField(default=True, verbose_name="Durum")

    def __str__(self):
        return self.product_name

    class Meta:
        verbose_name = 'Ürün'
        verbose_name_plural = 'Ürünler'


class cart(models.Model):
    product = models.ForeignKey(products, related_name="cartProduct", verbose_name="Ürün", on_delete=models.DO_NOTHING)
    adress = models.TextField(verbose_name="Adres")
    user_name = models.CharField(max_length=120, verbose_name="Kullanıcı Adı")

    def __str__(self):
        return self.user_name

    class Meta:
        verbose_name = 'Sepet'
        verbose_name_plural = 'Sepetler'
