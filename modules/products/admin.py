from django.contrib import admin
from modules.products.models import products, cart


# Register your models here.


class productsAdmin(admin.ModelAdmin):
    list_display = ('product_name', 'product_price','status')
    list_editable = ('status',)
    list_filter = ('status',)
    search_fields = ('product_name','product_price')

admin.site.register(products, productsAdmin)



class cartAdmin(admin.ModelAdmin):
    list_display = ('user_name',)

admin.site.register(cart, cartAdmin)
