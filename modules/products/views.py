import json

from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist
from django.core.serializers.json import DjangoJSONEncoder
from django.http import HttpResponse
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt

from modules.products.models import products


def json_response(status=False, message="", data={}):
    JsonData = {"status": status, "message": message, "data": data}
    JsonData = json.dumps(JsonData, cls=DjangoJSONEncoder)
    return HttpResponse(JsonData)


def getProducts(request):
    print(request.GET.get('id'))
    print(request.GET.get('name'))
    # _product = products.objects.get(status=True)
    _productsFilter = products.objects.filter(status=False)
    _productsAll = products.objects.all()

    # newProduct = products(
    #     product_name='Yeni ürün',
    #     product_code='123',
    #     product_price=123,
    #     status=True
    # )
    #
    # newProduct.save()

    # print(_product)
    print(_productsFilter)
    print(_productsAll)
    # for item in _productsFilter:
    #     print(item.product_name)
    #     print(item.product_code)
    #     print(item.product_price)
    #     print(item.status)

    return render(request, 'index.html', {"products": _productsAll})


@csrf_exempt
def get_products_api(request):
    status = False
    message = ""
    data = []
    keyword = request.POST.get('keyword')
    status = request.POST.get('status')
    print(keyword)
    print(status)
    try:
        if keyword:
            allProducts = products.objects.filter(product_name__icontains=keyword, status=status)
        else:
            allProducts = products.objects.all()
        for item in allProducts:
            data.append({
                "urun_ismi": item.product_name,
                "urun_fiyat": item.product_price,
                "urun_kodu": item.product_code,
                "urun_durumu": "Aktif" if item.status else "Pasif"
            })
        status = True
    except ObjectDoesNotExist:
        message = 'Bu ürün Bulunamadı'
    except Exception as e:
        message = str(e)

    return json_response(status=status, message=message, data=data)
