from django.shortcuts import render


def buildMasterpage(request):
    return render(request, 'index.html', {})


def testPage(request):
    return render(request, 'test.html', {})
