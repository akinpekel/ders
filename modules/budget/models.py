from django.db import models
from django.contrib.auth.models import User as AuthUser

TYPES = (
    ('income', 'Gelir'),
    ('outcome', 'Gider')
)

class Budgets(models.Model):
    user = models.ForeignKey(AuthUser, related_name="budgetsUsers", verbose_name="Kullanıcı", on_delete=models.PROTECT)
    type = models.CharField(max_length=30, choices=TYPES, verbose_name="Tip")
    amount = models.IntegerField(verbose_name="Miktar")

    def __str__(self):
        return self.user.username


class budgetData(models.Model):
    budget = models.ForeignKey(Budgets, related_name="badgetBadgetData", verbose_name="Budget",
                               on_delete=models.DO_NOTHING)
    key = models.CharField(max_length=100, verbose_name='Key')
    value = models.CharField(max_length=100, verbose_name='Value')




class settings(models.Model):
    user = models.ForeignKey(AuthUser, related_name="settingUser", verbose_name="Kullanıcı", on_delete=models.PROTECT)


    class Meta:
        verbose_name_plural = 'Settings'
        verbose_name = 'Setting'


class settingData(models.Model):
    setting = models.ForeignKey(settings, related_name="settingDataUser", verbose_name="Setting", on_delete=models.PROTECT)
    key = models.CharField(max_length=100,verbose_name='Key')
    value = models.CharField(max_length=100,verbose_name='Value')


    class Meta:
        verbose_name_plural = 'Setting Datas'
        verbose_name = 'Setting Data'